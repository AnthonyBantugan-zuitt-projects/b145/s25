// CRUD Operation

// Create
// - to insert documents

// insertOne Method
db.users.insertOne({document})

db.users.insertOne(
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "898923489",
			email: "janedoe@gmail.com"
		},
		courses: ["CSS", "JavaScript", "Pyhton"],
		department: "none"
	}
	)
	
	// InsertMany Method
	db.collections.insertMany([{doc1}, {doc2}, ...])
	
	db.users.insertMany(
		[
			{
				firstName: "Stephen",
				lastName: "Hawking",
				age: 21,
				contact: {
					phone: "89892319",
					email: "stephenhawking@gmail.com"
				},
				courses: ["React", "PHP", "Python"],
				department: "none"
			},
			{
				firstName: "Neil",
				lastName: "Armstrong",
				age: 21,
				contact: {
					phone: "767423489",
					email: "neilarmostrong@gmail.com"
				},
				courses: ["C++", "JavaScript", "Python"],
				department: "none"
			}
		]
		)
		
		// Read Operation
		// - retrieves documents from the collection
		
		db.collection.find({query}, {field projection})
		
		// find() method:
		db.users.find();
		
		// Update Operation
		// - update a document/s
		db.collections.updateOne()
		
		// add first a document to be modified
		db.users.insertOne(
			
			{
				firstName: "Test",
				lastName: "Test",
				age: 0,
				contact: {
					phone: "",
					email: ""
				},
				courses: [],
				department: ""
			}
			);
			
			// modifying the added document using update() method
			db.users.updateOne(
				{"firstName":"Test"},
				{$set: {
					"firstName": "Bill",
					"lastName": "Gates",
					"age": 65,
					"contact": {
						"phone": "12345678",
						"email": "bill@gmail.com"
					},
					"courses": ["PHP", "laravel","HTML"],
					"department": "Operations",
					"status": "active"
				}}
				)
				
				// use first name field as filter and look for the name "test".
				// using update operator set, update fields of the matching document with the following details:
				// bill gates, 65yo, phone: 12345678, bill@gmail.com, courses: PHP, laravel,HTML, operation dept. status:active
				
				// UpdateMany
				
				db.users.updateMany(
					{"department": "none"},
					{
						$set: {
							"department": "HR"
						}
					}
					)
					
					db.users.updateOne(
						{
							"_id" : ObjectId("61e7fda8c8d76cdf424f2e64")
						},
						{
							$unset: {
								"status": "active"
							}
						}
						
						)

// Delete Operation
	// - delete a document/s

	db.collections.deleteOn()
	db.collections.deleteMany()


	// insert a document as an example to be deleted
	db.users.insertOne(
		{
			firstName: "Joy",
		}
	)

	// deleteOne() method
	db.users.deleteOne(
		{
			"firstname": "Joy"
		}
	)
			
	// insert a new document with the name billy crawford
	db.users.insertOne(
		{
			firstName: "Bill",
			lastName: "Crawford"
		}
	)

	// deleteMany() method
	db.users.deleteMany(
		{
			"firstName": "Bill"
		}
	)
						